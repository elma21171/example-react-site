import React, { useState, useEffect } from "react";
import { Routes, Route } from "react-router-dom";
import { PageProvider } from "./contexts/PageContext";
import axios from "axios";
import Navbar from "./components/Navbar";
import Home from "./Home";
import Page1 from "./Page1";
import Page2 from "./Page2";
import NotFound from "./NotFound";
import "./App.css";

function App() {
  // const [data, setData] = useState();
  // const getData = async () => {
  //   const data = await axios.get();
  // };
  // useEffect(() => {
  //   getData();
  // }, []);

  // setData(data);


  return (
    <>
      <Navbar />
      <PageProvider>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/home" element={<Home />} />
          <Route path="/page1" element={<Page1 />} />
          <Route path="/page2" element={<Page2 />} />
          <Route path="/*" element={<NotFound />} />
        </Routes>
      </PageProvider>
    </>
  );
}

export default App;
