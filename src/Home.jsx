import React, { useContext } from "react";
import { PageContext } from "./contexts/PageContext";

function Home() {
  const { someValue, changeValue } = useContext(PageContext);
  return (
    <div style={{ backgroundColor: someValue ? "lightgreen" : "white"}}>
      <h1>Home page</h1>
      <button onClick={changeValue}>change background color</button>
    </div>
  );
}

export default React.memo(Home);
