import { Link } from "react-router-dom";

function NotFound() {
  return (
    <div>
      <h1>This page doesn't found</h1>
      <Link to="/">Go to Home page</Link>
    </div>
  );
}

export default NotFound;
