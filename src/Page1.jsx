import React, { useContext } from "react";
import { PageContext } from "./contexts/PageContext";

function Page1() {
  const { someValue, changeValue } = useContext(PageContext);

  return (
   <div style={{ backgroundColor: someValue ? "lightblue" : "pink", height: '100%'}}>
    <h1>page 1</h1>
    <button onClick={changeValue}>change background color</button>
   </div>
  );
}

export default React.memo(Page1);
