import React, { useContext } from "react";
import { PageContext } from "./contexts/PageContext";

function Page2() {
  const { someValue, changeValue } = useContext(PageContext);

  return (
    <div style={{ backgroundColor: someValue ? "lightyellow" : "brown", height: '100%'}}>
      <h1>page 2</h1>
      <button onClick={changeValue}>change background color</button>
    </div>
  );
}

export default React.memo(Page2);
