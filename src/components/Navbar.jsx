import { NavLink } from "react-router-dom";

function Navbar() {
  return (
    <header
      style={{
        backgroundColor: "lightgray",
        padding: 20,
        display: "flex",
        justifyContent: "space-between",
      }}
    >
      <NavLink to="/home">Home</NavLink>
      <NavLink to="/page1">Page 1</NavLink>
      <NavLink to="/page2">Page 2</NavLink>
    </header>
  );
}

export default Navbar;
