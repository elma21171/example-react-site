import { createContext, useState } from "react";

export const PageContext = createContext();

export function PageProvider({ children }) {
  const [someValue, setSomeValue] = useState(false);
  const changeValue = () => setSomeValue(!someValue);

  return (
    <PageContext.Provider value={{ someValue, changeValue }}>
      {children}
    </PageContext.Provider>
  );
}
